import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:seti_inno/createPage1.dart';
import 'package:seti_inno/createPage2.dart';

import 'confirm.dart';
import 'createPage3.dart';
import 'mainpage.dart';

final positiveEffect = TextEditingController();

class CreateComplete extends StatefulWidget {
  const CreateComplete({Key? key}) : super(key: key);

  @override
  State<CreateComplete> createState() => _CreateCompleteState();
}

class _CreateCompleteState extends State<CreateComplete> {
  bool isLoading = false;

  Future<bool> addQuest() async {
    setState(() {
      isLoading = true;
    });
    const url = 'http://phystechlab.ru/rosseti/public/api/suggestions/store';
    final request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers['Accept'] = 'application/json';
    request.headers['Authorization'] = 'Bearer $token';
    request.headers['Content-Type'] = 'multipart/form-data';
    request.fields['title'] = titles.text.trim();
    request.fields['topic_id'] = topicId;
    request.fields['existing_solution_text'] = forNow.text.trim();
    request.files.add(await http.MultipartFile.fromPath(
        'existing_solution_image', forNowPic.path));
    request.files.add(await http.MultipartFile.fromPath(
        'existing_solution_video', forNowVideo.path));
    request.fields['proposed_solution_text'] = forHow.text.trim();
    request.files.add(await http.MultipartFile.fromPath(
        'proposed_solution_image', forHowPic.path));
    request.files.add(await http.MultipartFile.fromPath(
        'proposed_solution_video', forHowVideo.path));
    request.fields['positive_effect'] = positiveEffect.text.trim();

    var streamedResponse = await request.send();
    setState(() {
      isLoading = false;
    });

    if (streamedResponse.statusCode == 200) {
      var response = await http.Response.fromStream(streamedResponse);
      log(streamedResponse.statusCode.toString());
      return true;
    } else {
      log(streamedResponse.statusCode.toString());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Wrap(runSpacing: 1, children: [
        Container(
          margin: const EdgeInsets.only(top: 72),
          width: 40,
          height: 40,
          child: MaterialButton(
            child: const Image(
              image: AssetImage('assets/vector.png'),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        Container(
          width: 140,
          height: 50,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 65, left: 90),
          child: const Text(
            'Создать',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 36,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 60,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 50, top: 70),
          child: const Image(image: AssetImage('assets/mini_logo.png')),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40),
          child: const Text(
            textAlign: TextAlign.center,
            'Расскажите как будeт',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 305,
          height: 292,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40, top: 20),
          child: SizedBox(
            height: 292,
            width: 305,
            child: TextField(
              expands: true,
              maxLines: null,
              controller: positiveEffect,
              decoration: const InputDecoration(
                counterText: "",
                fillColor: Colors.white,
                filled: true,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                  borderRadius: BorderRadius.all(
                    Radius.circular(24),
                  ),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                  borderRadius: BorderRadius.all(
                    Radius.circular(24),
                  ),
                ),
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40, top: 36),
          width: 305,
          height: 58,
          child: Material(
            elevation: 10,
            shadowColor: const Color(0xFFf4f5f6).withOpacity(0.7),
            borderRadius: BorderRadius.circular(24),
            child: MaterialButton(
              onPressed: () async {
                bool isFine = await addQuest();
                if (isFine == true) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => const MainPage()),
                      (Route<dynamic> route) => false);
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      duration: const Duration(seconds: 1),
                      backgroundColor: Colors.red,
                      content: Row(children: const [
                        SizedBox(width: 20),
                        Icon(Icons.error_outline, color: Colors.white),
                        SizedBox(width: 10),
                        Text('Что-то пошло не так!',
                            style: TextStyle(color: Colors.white))
                      ]),
                    ),
                  );
                }
              },
              child: isLoading
                  ? const CircularProgressIndicator()
                  : const Text(
                      'Готово',
                      style: TextStyle(
                          fontFamily: 'ABeeZee',
                          fontSize: 20,
                          height: 1.2,
                          color: Color(0xFF205692)),
                    ),
            ),
          ),
        ),
      ]),
    );
  }
}
