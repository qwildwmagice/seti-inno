import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:seti_inno/mainpage.dart';
import 'package:seti_inno/phone.dart';

final confNumberController = TextEditingController();

String token = '';

class PhoneConfirm extends StatefulWidget {
  const PhoneConfirm({Key? key}) : super(key: key);

  @override
  State<PhoneConfirm> createState() => _PhoneConfirmState();
}

class _PhoneConfirmState extends State<PhoneConfirm> {
  bool isLoading = false;

  Future<bool> phoneConf() async {
    setState(() {
      isLoading = true;
    });
    const url = 'https://phystechlab.ru/rosseti/public/api/auth/verify-code';
    final request = http.MultipartRequest('POST', Uri.parse(url));
    request.headers['Content-Type'] = 'multipart/form-data';
    request.headers['Accept'] = 'application/json';
    request.fields['phone'] = "+7${phoneNumberController.text.trim()}";
    request.fields['code'] = confNumberController.text.trim();
    var streamedResponse = await request.send();
    setState(() {
      isLoading = false;
    });

    if (streamedResponse.statusCode == 200) {
      var response = await http.Response.fromStream(streamedResponse);
      token = jsonDecode(response.body)['token'];
      print("Ok");
      log(token);
      return true;
    } else {
      log(streamedResponse.statusCode.toString());
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Center(
        child: Wrap(
          runSpacing: 17,
          children: [
            Material(
              elevation: 12,
              shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
              borderRadius: const BorderRadius.all(Radius.circular(24)),
              child: Container(
                color: Colors.transparent,
                width: 305,
                height: 58,
                child: TextField(
                  controller: confNumberController,
                  maxLength: 4,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: const InputDecoration(
                    counterText: "",
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'Код из СМС',
                    hintStyle: TextStyle(color: Colors.grey),
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.all(
                          Radius.circular(24),
                        )),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.all(
                          Radius.circular(24),
                        )),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 305,
              height: 58,
              child: Material(
                elevation: 8,
                shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
                borderRadius: BorderRadius.circular(24),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                      side: const BorderSide(color: Colors.white, width: 0)),
                  onPressed: () async {
                    bool isAuth = await phoneConf();
                    if (isAuth == true) {
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  const MainPage()),
                          (Route<dynamic> route) => false);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(
                          duration: const Duration(seconds: 1),
                          backgroundColor: Colors.red,
                          content: Row(children: const [
                            SizedBox(width: 20),
                            Icon(Icons.error_outline, color: Colors.white),
                            SizedBox(width: 10),
                            Text('Что-то пошло не так!',
                                style: TextStyle(color: Colors.white))
                          ]),
                        ),
                      );
                    }
                  },
                  child: isLoading
                      ? const CircularProgressIndicator()
                      : const Text(
                          'Далее',
                          style: TextStyle(
                              fontFamily: 'ABeeZee',
                              fontSize: 20,
                              height: 1.2,
                              color: Color(0xFF205692)),
                        ),
                ),
              ),
            ),
            SizedBox(
              width: 305,
              height: 58,
              child: Material(
                elevation: 8,
                shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
                borderRadius: BorderRadius.circular(24),
                child: MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24),
                      side: const BorderSide(color: Colors.white, width: 0)),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text(
                    'Назад',
                    style: TextStyle(
                        fontFamily: 'ABeeZee',
                        fontSize: 20,
                        height: 1.2,
                        color: Color(0xFF205692)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
