import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:seti_inno/confirm.dart';

final phoneNumberController = TextEditingController();
bool isLoading = false;

class PhoneHome extends StatefulWidget {
  const PhoneHome({Key? key}) : super(key: key);

  @override
  State<PhoneHome> createState() => _PhoneHomeState();
}

class _PhoneHomeState extends State<PhoneHome> {
  bool isLoading = false;

  Future<bool> phoneAuth() async {
    setState(() => isLoading = true);
    const url = 'https://phystechlab.ru/rosseti/public/api/auth/phone';
    final jsonBody = {'phone': "+7${phoneNumberController.text.trim()}"};
    final response = await http.post(
      Uri.parse(url),
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json',
      },
      body: jsonEncode(jsonBody),
    );
    final responseData = jsonDecode(response.body);
    setState(() => isLoading = false);
    print(responseData);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    TextField.noMaxLength;
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Center(
        child: SingleChildScrollView(
          child: Wrap(
            runSpacing: 17,
            children: [
              Material(
                elevation: 12,
                shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
                borderRadius: BorderRadius.circular(24),
                child: Container(
                  color: Colors.transparent,
                  width: 305,
                  height: 58,
                  child: TextField(
                    controller: phoneNumberController,
                    maxLength: 11,
                    keyboardType: TextInputType.phone,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    decoration: const InputDecoration(
                      counterText: "",
                      fillColor: Colors.white,
                      filled: true,
                      hintText: 'Телефон (бeз кода страны)',
                      hintStyle: TextStyle(color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.all(
                            Radius.circular(24),
                          )),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.all(
                            Radius.circular(24),
                          )),
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: 305,
                height: 58,
                child: Material(
                  elevation: 8,
                  shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
                  borderRadius: BorderRadius.circular(24),
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                        side: const BorderSide(color: Colors.white, width: 0)),
                    onPressed: () async {
                      final bool isAuth = await phoneAuth();
                      if (isAuth == true) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    const PhoneConfirm()));
                      } else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            duration: const Duration(seconds: 1),
                            backgroundColor: Colors.red,
                            content: Row(children: const [
                              SizedBox(width: 20),
                              Icon(Icons.error_outline, color: Colors.white),
                              SizedBox(width: 10),
                              Text('Что-то пошло не так!',
                                  style: TextStyle(color: Colors.white))
                            ])));
                      }
                    },
                    child: isLoading
                        ? const CircularProgressIndicator()
                        : const Text(
                            'Далее',
                            style: TextStyle(
                                fontFamily: 'ABeeZee',
                                fontSize: 20,
                                height: 1.2,
                                color: Color(0xFF205692)),
                          ),
                  ),
                ),
              ),
              SizedBox(
                width: 305,
                height: 58,
                child: Material(
                  elevation: 8,
                  shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
                  borderRadius: BorderRadius.circular(24),
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                        side: const BorderSide(color: Colors.white, width: 0)),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Назад',
                      style: TextStyle(
                          fontFamily: 'ABeeZee',
                          fontSize: 20,
                          height: 1.2,
                          color: Color(0xFF205692)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
