import 'package:flutter/material.dart';
import 'package:seti_inno/phone.dart';

//

void main() {
  runApp(MaterialApp(
    initialRoute: '/',
    debugShowCheckedModeBanner: false,
    routes: {
      '/': (context) => const MyApp(),
    },
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Center(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 151),
              child: const Image(
                image: AssetImage('assets/logo.png'),
                width: 141,
                height: 141,
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 40, bottom: 10),
              child: const Text('seti.inno',
                  style: TextStyle(
                      fontSize: 62,
                      height: 1.19,
                      color: Color(0xFF205692),
                      fontFamily: 'ABeeZee',
                      letterSpacing: -0.64)),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 180),
              child: const Text(
                'Рационализатор',
                style: TextStyle(
                    fontSize: 20,
                    height: 1.2,
                    color: Color(0xFF2C2929),
                    fontFamily: 'ABeeZee',
                    letterSpacing: -0.64),
              ),
            ),
            SizedBox(
              width: 305,
              height: 58,
              child: Material(
                elevation: 11,
                shadowColor: const Color(0xFFf4f5f6).withOpacity(0.7),
                borderRadius: BorderRadius.circular(24),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const PhoneHome()));
                  },
                  child: const Text(
                    'Регистрация',
                    style: TextStyle(
                        fontFamily: 'ABeeZee',
                        fontSize: 20,
                        height: 1.2,
                        color: Color(0xFF205692)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
