import 'package:flutter/material.dart';
import 'package:seti_inno/createPage2.dart';

final titles = TextEditingController();
late String topicId;

class CreatePage1 extends StatelessWidget {
  const CreatePage1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Wrap(runSpacing: 1, children: [
        Container(
          margin: const EdgeInsets.only(top: 72),
          width: 40,
          height: 40,
          child: MaterialButton(
            child: const Image(
              image: AssetImage('assets/vector.png'),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        Container(
          width: 140,
          height: 50,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 65, left: 90),
          child: const Text(
            'Создать',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 36,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 60,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 50, top: 70),
          child: const Image(image: AssetImage('assets/mini_logo.png')),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40),
          child: const Text(
            textAlign: TextAlign.center,
            'Расскажите о предложении',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40, top: 26),
          child: const Text(
            textAlign: TextAlign.center,
            'Выберите тему и название',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 305,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40, top: 34),
          child: const DropdownButtonExample(),
        ),
        Container(
          width: 305,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40, top: 34),
          child: TextField(
            controller: titles,
            decoration: const InputDecoration(
              counterText: "",
              fillColor: Colors.white,
              filled: true,
              hintText: 'Названиe',
              hintStyle: TextStyle(color: Colors.grey),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                borderRadius: BorderRadius.all(
                  Radius.circular(24),
                ),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                borderRadius: BorderRadius.all(
                  Radius.circular(24),
                ),
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40, top: 210),
          width: 305,
          height: 58,
          child: Material(
            elevation: 10,
            shadowColor: const Color(0xFFf4f5f6).withOpacity(0.7),
            borderRadius: BorderRadius.circular(24),
            child: MaterialButton(
              onPressed: () {
                print(topicId);
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const CreatePage2()));
              },
              child: const Text(
                'Дальшe',
                style: TextStyle(
                    fontFamily: 'ABeeZee',
                    fontSize: 20,
                    height: 1.2,
                    color: Color(0xFF205692)),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}

const List<String> list = [
  'Тема проекта',
  'Эксплуатация подстанций (подстанционного оборудования)',
  'Эксплуатация магистральных сетей;',
  'Эксплуатация распределительных сетей',
  'Эксплуатация зданий, сооружений, специальной техники',
  'Капитальное строительство, реконструкция, проектирование',
  'Оперативно-диспетчерское управление',
  'Релейная защита и противоаварийная автоматика',
  'Информационные технологии, системы связи',
  'Мониторинг и диагностика',
  'Контроль качества и учёт электроэнергии',
  'Производственная безопасность и охрана труда',
  'Технологическое присоединение',
  'Аварийно-восстановительные работы',
  'Экология, энергоэффективность, снижение потерь',
  'Совершенствование системы управления',
  'Дополнительные (нетарифные) услуги',
];

class DropdownButtonExample extends StatefulWidget {
  const DropdownButtonExample({super.key});

  @override
  State<DropdownButtonExample> createState() => _DropdownButtonExampleState();
}

class _DropdownButtonExampleState extends State<DropdownButtonExample> {
  String dropdownValue = list.first;

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: const InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF205692), width: 2),
          borderRadius: BorderRadius.all(
            Radius.circular(24),
          ),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF205692), width: 2),
          borderRadius: BorderRadius.all(
            Radius.circular(24),
          ),
        ),
      ),
      child: DropdownButtonHideUnderline(
        child: DropdownButton<String>(
          isExpanded: true,
          value: dropdownValue,
          icon: const Icon(Icons.arrow_downward),
          elevation: 16,
          style: const TextStyle(color: Color(0xFF205692)),
          onChanged: (String? value) {
            // This is called when the user selects an item.
            setState(() {
              dropdownValue = value!;
              topicId = list.indexOf(value).toString();
            });
          },
          items: list.map<DropdownMenuItem<String>>((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: Text(
                value,
                overflow: TextOverflow.ellipsis,
              ),
            );
          }).toList(),
        ),
      ),
    );
  }
}
