import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:seti_inno/createPage3.dart';
import 'package:video_player/video_player.dart';

final ImagePicker _picker = ImagePicker();
final forNow = TextEditingController();
late File forNowPic;
late File forNowVideo;

class CreatePage2 extends StatefulWidget {
  const CreatePage2({Key? key}) : super(key: key);

  @override
  State<CreatePage2> createState() => _CreatePage2State();
}

class _CreatePage2State extends State<CreatePage2> {
  late VideoPlayerController _controller;
  Future<File>? _image;
  Future<File>? _video;

  @override
  void initState() {
    super.initState();
  }

  Future<File> _getImage() async {
    final pickedImage = await _picker
        .pickImage(source: ImageSource.gallery)
        .whenComplete(() => {setState(() {})});
    return File(pickedImage!.path);
  }

  Future<File> _getVideo() async {
    final pickedVideo = await _picker
        .pickVideo(source: ImageSource.gallery)
        .whenComplete(() => {setState(() {})});
    _controller = VideoPlayerController.file(File(pickedVideo!.path))
      ..initialize().then((_) {
        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });
    return File(pickedVideo.path);
  }

  @override
  void dispose() {
    // Ensure disposing of the VideoPlayerController to free up resources.
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Wrap(runSpacing: 1, children: [
        Container(
          margin: const EdgeInsets.only(top: 72),
          width: 40,
          height: 40,
          child: MaterialButton(
            child: const Image(
              image: AssetImage('assets/vector.png'),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        Container(
          width: 140,
          height: 50,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 65, left: 90),
          child: const Text(
            'Создать',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 36,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 60,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 50, top: 70),
          child: const Image(image: AssetImage('assets/mini_logo.png')),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40),
          child: const Text(
            textAlign: TextAlign.center,
            'Расскажите как сeйчас',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 305,
          height: 292,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 40, top: 20),
          child: SizedBox(
            height: 292,
            width: 305,
            child: TextField(
              expands: true,
              maxLines: null,
              controller: forNow,
              decoration: const InputDecoration(
                counterText: "",
                fillColor: Colors.white,
                filled: true,
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                  borderRadius: BorderRadius.all(
                    Radius.circular(24),
                  ),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xFF205692), width: 2),
                  borderRadius: BorderRadius.all(
                    Radius.circular(24),
                  ),
                ),
              ),
            ),
          ),
        ),
        Container(
          width: 500,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 20),
          child: const Text(
            textAlign: TextAlign.center,
            'Добавьтe фото или видeо',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        FutureBuilder<File>(
            future: _video,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                forNowVideo = snapshot.data as File;
                return Container(
                  margin: const EdgeInsets.only(left: 35, top: 24),
                  width: 91,
                  height: 51,
                  child: MaterialButton(
                    onPressed: () {
                      setState(() {
                        _controller.value.isPlaying
                            ? _controller.pause()
                            : _controller.play();
                      });
                    },
                    child: _controller.value.isInitialized
                        ? AspectRatio(
                            aspectRatio: _controller.value.aspectRatio,
                            child: VideoPlayer(_controller),
                          )
                        : Container(),
                  ),
                );
              } else {
                return Container(
                  margin: const EdgeInsets.only(left: 35, top: 24),
                  width: 91,
                  height: 51,
                  child: const Icon(Icons.add),
                );
              }
            }),
        FutureBuilder<File>(
            future: _image,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                forNowPic = snapshot.data as File;
                return Container(
                  margin: const EdgeInsets.only(left: 14, top: 24),
                  width: 91,
                  height: 51,
                  child: Image(
                    image: FileImage(snapshot.data as File),
                  ),
                );
              } else {
                return Container(
                  margin: const EdgeInsets.only(left: 14, top: 24),
                  width: 91,
                  height: 51,
                  child: const Icon(Icons.add_a_photo),
                );
              }
            }),
        Container(
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 20, left: 14),
          width: 43,
          height: 43,
          child: MaterialButton(
            onPressed: () {
              setState(() {
                _video = _getVideo();
              });
            },
            color: Colors.white,
            textColor: Colors.black,
            padding: const EdgeInsets.all(10),
            shape: const CircleBorder(),
            child: const Icon(
              Icons.video_call,
              size: 24,
            ),
          ),
        ),
        Container(
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 12, top: 20),
          width: 43,
          height: 43,
          child: MaterialButton(
            onPressed: () {
              setState(() {
                _image = _getImage();
              });
            },
            color: Colors.white,
            textColor: Colors.black,
            padding: const EdgeInsets.all(10),
            shape: const CircleBorder(),
            child: const Icon(
              Icons.photo_camera,
              size: 24,
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40, top: 50),
          width: 305,
          height: 58,
          child: Material(
            elevation: 10,
            shadowColor: const Color(0xFFf4f5f6).withOpacity(0.7),
            borderRadius: BorderRadius.circular(24),
            child: MaterialButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const CreatePage3()));
              },
              child: const Text(
                'Дальшe',
                style: TextStyle(
                    fontFamily: 'ABeeZee',
                    fontSize: 20,
                    height: 1.2,
                    color: Color(0xFF205692)),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
