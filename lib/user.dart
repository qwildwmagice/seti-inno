import 'package:flutter/material.dart';
import 'package:seti_inno/mainpage.dart';

class UserStats extends StatelessWidget {
  const UserStats({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Wrap(runSpacing: 1, children: [
        Container(
          width: 190,
          height: 50,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 65, left: 95),
          child: const Text(
            'Мой статус',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 36,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 60,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 12, top: 70),
          child: const Image(image: AssetImage('assets/mini_logo.png')),
        ),
        Container(
          alignment: Alignment.topCenter,
          width: 125,
          height: 125,
          margin: const EdgeInsets.only(left: 126),
          child: const Image(
            image: AssetImage('assets/silver.png'),
          ),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 35),
          child: const Text(
            'Сeрeбряный статус',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 303,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 41),
          child: Row(children: [
            const Text(
              'Оценок',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
            const Spacer(),
            Text(
              '${userDetails.ratingsCount}',
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
          ]),
        ),
        Container(
          width: 303,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 41),
          child: Row(children: [
            const Text(
              'Комментариев',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
            const Spacer(),
            Text(
              '${userDetails.commentsCount}',
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
          ]),
        ),
        Container(
          width: 303,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 41),
          child: Row(children: [
            const Text(
              'Предложений',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
            const Spacer(),
            Text(
              '${userDetails.proposalsCount}',
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
          ]),
        ),
        Container(
          width: 303,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 41),
          child: Row(children: [
            const Text(
              'Одобрено',
              style: TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
            const Spacer(),
            Text(
              '${userDetails.acceptedProposalsCount}',
              style: const TextStyle(
                fontWeight: FontWeight.w400,
                letterSpacing: -0.7,
                fontSize: 20,
                fontFamily: 'ABeeZee',
                color: Color(0xFF205692),
              ),
            ),
          ]),
        ),
        Container(
          width: 303,
          height: 27,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 35, top: 10),
          child: const Text(
            'Итого 1300 бонусов',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 303,
          height: 96,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 35, top: 15),
          child: const Text(
            textAlign: TextAlign.center,
            'До золотого статуса\nеще 15 оценок или\n6 комментариев или 1\nпредложение',
            style: TextStyle(
              height: 1.15,
              fontWeight: FontWeight.w400,
              letterSpacing: -0.7,
              fontSize: 20,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(left: 40, top: 32),
          width: 305,
          height: 58,
          child: Material(
            elevation: 10,
            shadowColor: const Color(0xFFf4f5f6).withOpacity(0.7),
            borderRadius: BorderRadius.circular(24),
            child: MaterialButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => const MainPage()));
              },
              child: const Text(
                'Готово',
                style: TextStyle(
                    fontFamily: 'ABeeZee',
                    fontSize: 20,
                    height: 1.2,
                    color: Color(0xFF205692)),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
