import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:seti_inno/createPage1.dart';
import 'package:seti_inno/user.dart';

import 'confirm.dart';

late UserDetails userDetails;

class UserDetails {
  int? id;
  String? fullName;
  String? phone;
  Null? topicId;
  Null? email;
  int? commentsCount;
  int? ratingsCount;
  int? acceptedProposalsCount;
  int? deniedProposalsCount;
  int? proposalsCount;

  UserDetails(
      {this.id,
      this.fullName,
      this.phone,
      this.topicId,
      this.email,
      this.commentsCount,
      this.ratingsCount,
      this.acceptedProposalsCount,
      this.deniedProposalsCount,
      this.proposalsCount});

  UserDetails.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    phone = json['phone'];
    topicId = json['topic_id'];
    email = json['email'];
    commentsCount = json['comments_count'];
    ratingsCount = json['ratings_count'];
    acceptedProposalsCount = json['accepted_proposals_count'];
    deniedProposalsCount = json['denied_proposals_count'];
    proposalsCount = json['proposals_count'];
  }
}

class MainPage extends StatelessWidget {
  const MainPage({Key? key}) : super(key: key);

  Future<bool> userStats() async {
    Map<String, String> requestHeaders = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    final url = Uri.parse('https://phystechlab.ru/rosseti/public/api/user');
    final response = await http.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      print(response.body);
      userDetails = UserDetails.fromJson(jsonDecode(response.body));
      return true;
    } else {
      print(requestHeaders.toString());
      print(response.statusCode);
      return false;
    }
  }

  Future<bool> createTopics() async {
    Map<String, String> requestHeaders = {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    final url = Uri.parse('https://phystechlab.ru/rosseti/public/api/topics');
    final response = await http.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return true;
    } else {
      print(requestHeaders.toString());
      print(response.statusCode);
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFAFBFD),
      body: Wrap(children: [
        Container(
          width: 140,
          height: 43,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(top: 68, left: 121),
          child: const Text(
            'seti.inno',
            style: TextStyle(
              letterSpacing: -0.7,
              fontSize: 36,
              fontFamily: 'ABeeZee',
              color: Color(0xFF205692),
            ),
          ),
        ),
        Container(
          width: 80,
          height: 60,
          alignment: Alignment.topCenter,
          margin: const EdgeInsets.only(left: 50, top: 65),
          child: MaterialButton(
            onPressed: () async {
              final isSucceed = await userStats();
              if (isSucceed == true) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) => const UserStats()));
              } else {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    duration: const Duration(seconds: 1),
                    backgroundColor: Colors.red,
                    content: Row(children: const [
                      SizedBox(width: 20),
                      Icon(Icons.error_outline, color: Colors.white),
                      SizedBox(width: 10),
                      Text('Что-то пошло не так!',
                          style: TextStyle(color: Colors.white))
                    ]),
                  ),
                );
              }
            },
            child: const Image(image: AssetImage('assets/mini_logo.png')),
          ),
        ),
        Center(
          child: Container(
            margin: const EdgeInsets.only(top: 21),
            width: 305,
            height: 165,
            child: Material(
              elevation: 8,
              shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
              borderRadius: BorderRadius.circular(24),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  side: const BorderSide(color: Colors.white, width: 0),
                ),
                onPressed: () async {
                  final isSucceed = await createTopics();
                  if (isSucceed == true) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (BuildContext context) =>
                                const CreatePage1()));
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        duration: const Duration(seconds: 1),
                        backgroundColor: Colors.red,
                        content: Row(children: const [
                          SizedBox(width: 20),
                          Icon(Icons.error_outline, color: Colors.white),
                          SizedBox(width: 10),
                          Text('Что-то пошло не так!',
                              style: TextStyle(color: Colors.white))
                        ]),
                      ),
                    );
                  }
                },
                child: Wrap(
                  children: [
                    const SizedBox(
                      width: 92,
                      height: 92,
                      child: Image(
                        image: AssetImage('assets/main_button_1.png'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 24, top: 16),
                      width: 150,
                      height: 55,
                      child: const Text(
                        textAlign: TextAlign.center,
                        'Создать прeдложeниe',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'ABeeZee',
                          color: Color(0xFF205692),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Center(
          child: Container(
            margin: const EdgeInsets.only(top: 21),
            width: 305,
            height: 165,
            child: Material(
              elevation: 8,
              shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
              borderRadius: BorderRadius.circular(24),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  side: const BorderSide(color: Colors.white, width: 0),
                ),
                onPressed: () {},
                child: Wrap(
                  children: [
                    const SizedBox(
                      width: 92,
                      height: 92,
                      child: Image(
                        image: AssetImage('assets/main_button_2.png'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 25, top: 27),
                      width: 150,
                      height: 51,
                      child: const Text(
                        textAlign: TextAlign.center,
                        'Заявки',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'ABeeZee',
                          color: Color(0xFF205692),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Center(
          child: Container(
            margin: const EdgeInsets.only(top: 21),
            width: 305,
            height: 165,
            child: Material(
              elevation: 8,
              shadowColor: const Color(0xFFf4f5f6).withOpacity(0.8),
              borderRadius: BorderRadius.circular(24),
              child: MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                  side: const BorderSide(color: Colors.white, width: 0),
                ),
                onPressed: () {},
                child: Wrap(
                  children: [
                    const SizedBox(
                      width: 92,
                      height: 92,
                      child: Image(
                        image: AssetImage('assets/main_button_3.png'),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 25, top: 27),
                      width: 150,
                      height: 51,
                      child: const Text(
                        textAlign: TextAlign.center,
                        'Экспeртизы',
                        style: TextStyle(
                          fontSize: 20,
                          fontFamily: 'ABeeZee',
                          color: Color(0xFF205692),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ]),
    );
  }
}
